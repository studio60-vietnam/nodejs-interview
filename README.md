# Nodejs Test #

#### Test Description
Create a product landing page with features as below :

* A link to allow user add a new product.
* A table with 3 columns (Image, Title, Actions) to show 10 products with a **Load More** button, when user click on the **Load More** button then will load 10 next products and append to the table. Check if the web service return no result or less than 10 prodcuts then hide **Load More** button.
* With each product row, in the **Actions** column of the table has two links : **Edit** and **Delete** to allow user edit and delete a product.
* Create a new page with a form to allow user add or edit a product, the form fields as below :
    * Image (required) (optionally show progresss bar when file uploading)
    * Title (required)
    * Desc (required)
* Note : When user want to add, edit or delete a product then a login popup will be display to require user login before preforming that actions if user hasn't authenticated yet.

#### Technical requirement
* Database : mongodb
* Build the REST APIs to serve for this test application with nodejs and express framework.
* Optionally use redis cache
* Optionally run the application with cluster.
* Optionally add Unit Tests, for example as Karma or other.

#### Instructions
Fork this repository. Create a new branch to do your work. When done create a pull request back to studio60-vietnam as a new branch. If there are any questions, feel free to record any assumptions made.
